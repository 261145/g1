# G1

## Descrizione
Il gruppo 1 rende disponibile un applicativo desktop che va incontro alle esigenze del cliente.

L'idea è di dare la possibilità ad un utente di visualizzare, consultare i tweet (recuperati dalla piattaforma Twitter) con determinati hashtag e attivare un sistema di notifiche al verificarsi di determinate condizioni.
Sarà possibile classificarli in base: 
* all'area geografica
* a uno specifico punto d'interesse
* a una parola chiave
* all'utente che pubblica i tweet.

Inoltre, una volta scelto il tipo di classificazione, sarà possibile analizzarli ed estrapolare informazioni attraverso diverse rappresentazioni grafiche.

## Team

Il team è composto da 6 persone con competenze e interessi affini alla realizzazione della struttura, della logica e della GUI.

* ### **_(Matr. 141017)_ Fabio Morsiani**
  * Product Owner
<br/><br/>

* ### **_(Matr. 133356)_ Federico Gibertoni**
  * Scrum Master
<br/><br/>

* ### **_(Matr. 131035)_ Mattia Billa**
  * Developer
  * Sviluppo branch: GUI
<br/><br/>

* ### **_(Matr. 130795)_ Federico Scaltriti**
  * Developer
  * Sviluppo branch: backend
<br/><br/>

* ### **_(Matr. 132009)_ Lorenzo Storchi**
  * Developer
  * Sviluppo branch: backend
<br/><br/>

* ### **_(Matr. 136424)_ Matteo Faraci**
  * Developer
  * Sviluppo branch: GUI
<br/><br/>

## Installazione (necessaria versione di Python >= 3.7)
Recarsi nella cartella <i>release/</i> in cui sono riportati gli eseguibili per i seguenti sistemi operativi.

### Windows
Doppio click su ```install.bat``` per installare tutte le dipendenze richieste.
Doppio click su ```start.bat``` per lanciare l'applicazione.

### GNU/Linux
Digitare ```bash install.sh``` per installare tutte le dipendenze richieste.
Digitare ```bash start.sh``` per lanciare l'applicazione.

## Link CAS
* [_GitLab_](https://gitlab.com/261145/g1)
* [_SonarCloud_](https://sonarcloud.io/dashboard?id=261145_g1)
* [_Taiga_](https://tree.taiga.io/project/261145-g1/backlog)

## [Video di presentazione](https://drive.google.com/file/d/1Q14iiB6szb7KcKylEMRnRhWlNpN4M94V/view?usp=sharing)
