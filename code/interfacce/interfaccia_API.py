import datetime

import tweepy
from tweepy import API

from keys import *
from geopy.geocoders import Nominatim
from utils import utils


class InterfacciaAPI(API):

    def __init__(self, no_auth=False):
        if no_auth:
            super().__init__()
        else:
            auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
            auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
            super().__init__(auth)
        self.tweets = []
        self.range = {'inizio': datetime.date(2006, 3, 21), 'fine': datetime.date.today() + datetime.timedelta(days = 1) }

    def get_users_list(self, user_name, number_of_users=10):
        """
        :param user_name: stringa utente da cercare
        :param number_of_users: numero max utenti restituiti da search_users()
        """
        return self.search_users(user_name)[:number_of_users]

    def get_user_tweets(self, user_name, max_tweets=10):
        """
        :param user_name: stringa
        :param max_tweets: int, numero massimo di tweet da scaricare
        """
        self.tweets = []
        for status in tweepy.Cursor(self.user_timeline, id=user_name).items(max_tweets):
            if self.range['fine'] >= status.created_at.date() >= self.range['inizio']:
                self.tweets.append(status)

    def set_range(self, inizio, fine):
        """
        :param inizio: tupla (anno, mese, giorno)
        :param fine: tupla (anno, mese, giorno)
        """
        inizio = datetime.date(*inizio)
        fine = datetime.date(*fine)
        self.range = {'inizio': inizio, 'fine': fine}

    def get_tweets_by_content(self, content, max_tweets=10):
        self.tweets = []
        for status in tweepy.Cursor(self.search, q=content,
                                    since=self.range['inizio'],
                                    until=self.range['fine'], result_type="recent").items(max_tweets):
            self.tweets.append(status)

    def get_tweets_by_place(self, lat=None, long=None, place_name=None, radius=10, content="", max_tweets=10):
        self.tweets = []
        if place_name is not None:
            try:
                geolocator = Nominatim(user_agent="Tweet Eye")
                location = geolocator.geocode(place_name)
                lat = location.latitude
                long = location.longitude
            except AttributeError:
                return None

        for status in tweepy.Cursor(self.search, q=content,
                                    geocode=f"{lat},{long},{radius}km",
                                    since=self.range['inizio'],
                                    until=self.range['fine'], result_type="recent").items(max_tweets):
            self.tweets.append(status)

    def auto_post_on_twitter(self, tweets):
        # richiamo le funzioni di analisi statistica
        avg_time = utils.avg_time_tweets(tweets)
        if avg_time is not None:
            avg_time = round(avg_time, 2)
        geo_tweets = utils.num_geo_tweets(tweets)
        p_geo_tweets = utils.perc_geo_tweets(tweets)
        img_tweets = utils.num_img_tweets(tweets)

        # formatto un tweet apposta
        tweet_text = f"Analisi dei tweet completa:" \
                     f"\n- Tweet totali: {len(tweets)} " \
                     f"\n- Media oraria di tweet: {avg_time} " \
                     f"\n- Tweet geolocalizzati: {geo_tweets} " \
                     f"\n- Percentuale tweet geolocalizzati: {p_geo_tweets} " \
                     f"\n- Tweet contenenti immagini: {img_tweets}"

        # posto il tweet
        self.update_status(tweet_text)

    def auto_post_image(self, image, image_name="default-image", status=""):

        image.save(image_name+".PNG")

        self.update_with_media(image_name+".PNG", status)
