import tweepy
from tweepy import StreamListener

from keys import *


class StdOutListener(StreamListener):

    def __init__(self):
        super().__init__()
        self.tweets = []

    def on_status(self, status):
        self.tweets.append(status)
        return True


class InterfacciaRT(tweepy.Stream, StdOutListener):

    def __init__(self, no_auth=False):
        self.listener = StdOutListener()
        self._cont = 0
        if no_auth:
            super().__init__(tweepy.OAuthHandler("", ""), self.listener)
        else:
            auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
            auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
            super().__init__(auth, self.listener)

    def start_real_time(self, content, is_async=True):
        self.filter(track=[content], is_async=is_async)

    def get_new_tweets(self):
        new_tweets = []
        if self._cont != len(self.tweets):
            new_tweets = self.tweets[self._cont:]
            self._cont += len(new_tweets)
            return new_tweets
        else:
            return False

    @property
    def tweets(self):
        return self.listener.tweets

    def stop_real_time(self):
        if self.running:
            self.disconnect()
