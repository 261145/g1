from windows.menu import Menu

import sys
from PyQt5.QtWidgets import *
from qt_material import apply_stylesheet

if __name__ == '__main__':
    app = QApplication(sys.argv)

    page = Menu()

    # extra = {
    #     'danger': '#dc3545',
    #     'warning': '#ffc107',
    #     'success': '#17a2b8',
    # }

    apply_stylesheet(app, 'light_blue.xml')

    sys.exit(app.exec_())