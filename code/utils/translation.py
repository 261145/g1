tr_lang = "it"

tr_texts = {
    "text_error": {
        "it": "Errore",
        "en": "Error"
    },
    "text_success": {
        "it": "Successo",
        "en": "Success"
    },
    "text_start": {
        "it": "Avvia",
        "en": "Start"
    },
    "text_start_realtime": {
        "it": "Avvia Live",
        "en": "Start Live"
    },
    "text_interrupt": {
        "it": "Interrompi",
        "en": "Interrupt"
    },
    "text_save": {
        "it": "Salva",
        "en": "Save"
    },
    "text_load": {
        "it": "Carica File",
        "en": "Load File"
    },
    "text_monitor": {
        "it": "Monitora",
        "en": "Monitor"
    },
    "text_track": {
        "it": "Traccia Utente",
        "en": "Track User"
    },
    "text_track_placeholder": {
        "it": "Inserisci username da tracciare",
        "en": "Insert username to track"
    },
    "text_monitor_placeholder": {
        "it": "Inserisci tag da monitorare",
        "en": "Insert tag to monitor"
    },
    "text_user_not_found": {
        "it": "L'utente non è stato trovato",
        "en": "User not found"
    },
    "text_select_file": {
        "it": "Seleziona un salvataggio",
        "en": "Select a file"
    },
    "text_search_void": {
        "it": "Prima di salvare la ricerca deve aver prodotto dei risultati",
        "en": "Nothing to save"
    },
    "text_query_empty": {
        "it": "Il campo di ricerca è vuoto",
        "en": "Search field is empty"
    },
    "text_generate_cloud": {
        "it": "Genera nuvola",
        "en": "Generate cloud"
    },
    "text_generate_cloud_no_tweets": {
        "it": "Non ci sono hashtag da visualizzare",
        "en": "No hashtags to visualize"
    },
    "text_save_map": {
        "it": "Salva la mappa",
        "en": "Save map"
    },
    "text_generate_hist": {
        "it": "Genera grafici",
        "en": "Generate stats"
    },
    "text_place_radius": {
        "it": "Raggio in km",
        "en": "Radius in km"
    },
    "text_place_placeholder": {
        "it": "Inserisci la località",
        "en": "Insert place"
    },
    "text_alarm_placeholder": {
        "it": "T/H allarme",
        "en": "T/H alarm"
    },
    "text_alarm": {
        "it": "ALLARME",
        "en": "ALARM"
    },
    "text_alarm_description": {
        "it": "La soglia è stata superata!",
        "en": "Threshold has been exceeded!"
    },
    "text_share": {
        "it": "Condividi statistiche",
        "en": "Share statistics"
    },
    "text_confirm_share": {
        "it": "Condivisione effettuata!",
        "en": "Share succeeded"
    }
}