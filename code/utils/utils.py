import tweepy
import json
import wordcloud
import re
import matplotlib.pyplot as plt

from interfacce.interfaccia_API import InterfacciaAPI
from collections import Counter

def save_search(search_type, tweets, folder, file_name):
    """
    Function to save the user search in a JSON file
    :param search_type: either 'traccia', for search by user, or 'monitora' for search by hashtag
    :param tweets: the list of tweets
    :param folder: the chosen folder in which to save the file
    :param file_name: the file name
    """
    if file_name.endswith(".json"):
        file_name = file_name[:-5]

    with open(f"{folder}/{file_name}.json", 'w', encoding='utf-8') as file:
        json_structure = {
            'type': search_type,
            'tweets': [tweet._json for tweet in tweets]
        }
        json.dump(json_structure, file, ensure_ascii=False, indent=4)


def reload_search(file_path):
    """
    Function to reload the selected search file
    :param file_path: the path in which the file is saved + the file's name
    :return: a dictionary containing the JSON data
    """
    with open(file_path, "r", encoding='utf-8') as json_file:
        data = json.load(json_file)

    api = InterfacciaAPI()
    tweets = []
    for t in data["tweets"]:
        status = tweepy.Status.parse(api, t)
        tweets.append(status)

    return tweets, data["type"]


def get_geo_coordinates(tweet_data):
    parsed_data = dict(tweet_data._json)

    geo = parsed_data.get('coordinates', None)
    if geo is not None:
        return geo

    try:
        geo = tweet_data.retweeted_status.coordinates.get('coordinates')[::-1]
        if geo is not None:
            return geo
    except:
        pass

    try:
        geo = tweet_data.retweeted_status.place.bounding_box.coordinates[0][0][::-1]
        if geo is not None:
            return geo
    except:
        pass

    try:
        geo = tweet_data.place.bounding_box.coordinates[0][0][::-1]
        if geo is not None:
            return geo
    except:
        pass

    return None

def get_hashtags_list(tweets):
    hashtags = []

    for t in tweets:
        for h in t.entities["hashtags"]:
            hashtags.append(h["text"])

    return hashtags

def generate_wordcloud(tweets):
    text = get_hashtags_list(tweets)

    wc = wordcloud.WordCloud(background_color=None, mode="RGBA").generate(' '.join(text))

    return wc

def get_hashtags_ranking(tweets, topK=10):
    text = get_hashtags_list(tweets)

    return Counter(text).most_common(topK)

def generate_hashtag_histogram(tweets):
    dates = []
    plt.clf()
    for t in tweets:
        date = t.created_at
        dates.append(date)

    plt.xticks(rotation=45)
    plt.hist(dates)

    return plt


def avg_time_tweets(tweets):

    first_t = tweets[0].created_at
    last_t = tweets[len(tweets)-1].created_at
    time_range = first_t-last_t
    total_hours = time_range.days*24 + time_range.seconds/3600

    if total_hours == 0:
        return None

    return abs(len(tweets)/total_hours)


def num_geo_tweets(tweets):
    count = 0
    for tweet in tweets:
        if get_geo_coordinates(tweet) is not None:
            count += 1

    return count


def perc_geo_tweets(tweets):
    count = num_geo_tweets(tweets)

    if count == 0:
        return None

    return count/len(tweets)*100


def num_img_tweets(tweets):
    count = 0
    for tweet in tweets:
        if 'media' in tweet.entities:
            count += 1

    return count
