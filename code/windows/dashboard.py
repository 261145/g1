import typing

import PIL
from PyQt5.QtGui import QPixmap, QFont
from PyQt5.QtCore import Qt, QDateTime, QDate
import sys

from PyQt5.QtWidgets import QGridLayout, QLabel, QPushButton, QHBoxLayout, QLineEdit, QSpacerItem, QSizePolicy, QWidget, \
    QVBoxLayout, QScrollArea, QErrorMessage, QFileDialog, QApplication, QCalendarWidget, QDateEdit
from qt_material import apply_stylesheet

from PIL.ImageQt import ImageQt

from utils.utils import get_geo_coordinates, generate_wordcloud, get_hashtags_list
from utils.translation import tr_texts


from windows.mappa import Mappa

from utils.utils import generate_hashtag_histogram
import io

from utils.utils import get_hashtags_ranking, avg_time_tweets, num_geo_tweets, num_img_tweets, perc_geo_tweets
from interfacce.interfaccia_API import InterfacciaAPI


class Dashboard(QWidget):

    def __init__(self, parent: typing.Optional['QWidget'], flags: typing.Union[Qt.WindowFlags, Qt.WindowType], mode: str = None, lang="it") -> None:
        super(Dashboard, self).__init__(parent=parent, flags=flags)

        self.lang = lang
        # Set dimensions
        self.setWindowTitle('Tweet eye')
        self.resize(1200, 650)

        # Main layout
        self.gridLayout = QGridLayout()

        # Page label
        self.label = QLabel()

        # Add label to main layout
        self.gridLayout.addWidget(self.label, 0, 0, 1, 2)

        # Header buttons

        self.button_real_time = QPushButton()
        self.button_avvia = QPushButton()

        # Header layout
        self.header_horizontal_layout = QHBoxLayout()
        self.line_edit = QLineEdit()
        self.set_placeholder_text("suggerimento")
        self.header_horizontal_layout.addWidget(self.line_edit)

        self.date_range1 = QDateEdit()
        self.date_range1.dateChanged.connect(self.date_range1_validator)
        self.date_range2 = QDateEdit()
        self.date_range2.dateChanged.connect(self.date_range1_validator)
        self.date_range1.setDate(QDate.currentDate().addDays(-7))
        self.date_range1.setMinimumDate(QDate.currentDate().addDays(-7))
        self.date_range1.setMaximumDate(QDate.currentDate().addDays(1))
        self.date_range2.setDate(QDate.currentDate().addDays(1))
        self.date_range2.setMaximumDate(QDate.currentDate().addDays(1))

        self.header_horizontal_layout.addWidget(self.date_range1)
        self.header_horizontal_layout.addWidget(self.date_range2)



        self.header_horizontal_layout.addWidget(self.button_avvia)
        header_spacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.header_horizontal_layout.addItem(header_spacer)

        # Add header layout to main layout
        self.gridLayout.addLayout(self.header_horizontal_layout, 2, 0, 1, 2)

        # Map widget
        self.widget_map = Mappa(self)
        map_size_policy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        map_size_policy.setHorizontalStretch(0)
        map_size_policy.setVerticalStretch(0)
        map_size_policy.setHeightForWidth(self.widget_map.sizePolicy().hasHeightForWidth())
        self.widget_map.setSizePolicy(map_size_policy)

        # Widget that contains the collection of Vertical Box
        self.widget_tweets = QWidget()

        # The Vertical Box that contains the Horizontal Boxes of  labels and buttons
        self.tweet_container = QVBoxLayout()
        self.widget_tweets.setLayout(self.tweet_container)

        # Add tweet widget to scroll area
        self.scrollArea = QScrollArea()
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setWidget(self.widget_tweets)

        # Add tweets to main layout
        self.gridLayout.addWidget(self.scrollArea, 3, 0, 1, 1)

        # Map container
        self.map_and_buttons_container = QVBoxLayout()
        self.map_and_buttons_container.addWidget(self.widget_map)

        # Map buttons container
        self.map_buttons_container = QHBoxLayout()

        # Map buttons
        self.button_save_map = QPushButton()
        self.button_hist = QPushButton()
        self.button_cloud = QPushButton()
        self.button_salva = QPushButton()

        # Adding buttons to map buttons container
        self.map_buttons_container.addWidget(self.button_save_map)
        self.map_buttons_container.addWidget(self.button_hist)
        self.map_buttons_container.addWidget(self.button_cloud)
        self.map_buttons_container.addWidget(self.button_salva)
        map_spacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.map_buttons_container.addItem(map_spacer)

        # Add map buttons container to map and buttons container
        self.map_and_buttons_container.addLayout(self.map_buttons_container)

        # Add map and buttons to main layout
        self.gridLayout.addLayout(self.map_and_buttons_container, 3, 1, 1, 1)

        # Events
        self.widget_map.loadFinished.connect(self.widget_map_load_finished)

        # Button events
        self.button_hist.clicked.connect(self.display_hist)
        self.button_save_map.clicked.connect(self.button_save_map_event)
        self.button_cloud.clicked.connect(self.display_cloud)
        self.button_salva.clicked.connect(self.button_salva_event)
        self.button_avvia.clicked.connect(self.button_avvia_event)

        # Set main layout
        self.setLayout(self.gridLayout)

        self.setAttribute(Qt.WidgetAttribute.WA_DeleteOnClose)
        self.show()  # Show the GUI

        self.line_edit_error_message = None
        self.tweets_error_message = None

        self.set_text()
        self.set_title_text("Label")

        self.tweets = []
        self.tweets_data = []
        self.mode = mode
        self.marker_type = None

    def set_title_text(self, text):
        self.label.setText(text)
        self.label.setFont(QFont('Arial', 32))

    def set_placeholder_text(self, text):
        self.line_edit.setPlaceholderText(text)


    def widget_map_load_finished(self, ok):
        if ok:
            pass
            # Add coordinates here -> self.widget_map.add_marker(coordinates)

    def set_text(self):
        self.button_save_map.setText(tr_texts["text_save_map"][self.lang])
        self.button_hist.setText(tr_texts["text_generate_hist"][self.lang])
        self.button_cloud.setText(tr_texts["text_generate_cloud"][self.lang])
        self.button_salva.setText(tr_texts["text_save"][self.lang])

        self.button_real_time.setText("PushButton")
        self.button_avvia.setText(tr_texts["text_start"][self.lang])

    def remove_tweets(self):
        for t in self.tweets:
            self.tweet_container.removeWidget(t)
        self.tweets = []
        self.tweets_data = []


    def add_tweet(self, tweet, tweet_data):
        self.tweets.append(tweet)
        self.tweet_container.addWidget(tweet)
        self.tweets_data.append(tweet_data)
        self.add_marker(tweet, tweet_data)

    def add_tweet_reverse(self, tweet, tweet_data):
        self.tweets.append(tweet)
        self.tweet_container.insertWidget(0,tweet)
        self.tweets_data.append(tweet_data)
        self.add_marker(tweet, tweet_data)

    def add_marker(self, tweet, tweet_data):
        coo = get_geo_coordinates(tweet_data)
        if coo is not None:
            self.widget_map.add_marker(coo)


    def remove_markers(self):
        self.widget_map.remove_markers()

    def remove_polyline(self):
        self.widget_map.remove_polyline()

    def button_salva_event(self):
        if len(self.tweets) == 0:
            self.tweets_error_message = self.tweets_error_message or QErrorMessage(self)
            self.tweets_error_message.setWindowTitle(tr_texts["text_error"][self.lang])
            self.tweets_error_message.showMessage(tr_texts["text_search_void"][self.lang])
            return None

        file_name, _ = QFileDialog.getSaveFileName(self, tr_texts["text_save"][self.lang], options=QFileDialog.DontUseNativeDialog) \
            if self.mode != 'testing' else ('test', None)
        return file_name

    def button_avvia_event(self):

        input_text = self.line_edit.text()
        if not input_text:
            self.line_edit_error_message = self.line_edit_error_message or QErrorMessage(self)
            self.line_edit_error_message.setWindowTitle(tr_texts["text_error"][self.lang])
            self.line_edit_error_message.showMessage(tr_texts["text_query_empty"][self.lang])
            return False
        else:
            return True

    def display_cloud(self):


        if len(self.tweets_data) == 0 or len(get_hashtags_list(self.tweets_data)) == 0:
            self.tweets_error_message = self.tweets_error_message or QErrorMessage(self)
            self.tweets_error_message.setWindowTitle(tr_texts["text_error"][self.lang])
            self.tweets_error_message.showMessage(tr_texts["text_generate_cloud_no_tweets"][self.lang])
            return None
        self.cloud = Cloud(self, Qt.WindowType.Window, self.tweets_data, lang=self.lang)
        self.cloud.show()

    def display_hist(self):
        if len(self.tweets_data) == 0:
            self.tweets_error_message = self.tweets_error_message or QErrorMessage(self)
            self.tweets_error_message.setWindowTitle(tr_texts["text_error"][self.lang])
            self.tweets_error_message.showMessage(tr_texts["text_generate_cloud_no_tweets"][self.lang])
            return None
        self.stats = Stats(self, Qt.WindowType.Window, self.tweets_data, lang=self.lang)
        self.stats.show()

    def button_save_map_event(self):
        self.widget_map.page().toHtml(self.on_save_map_event)

    def get_marker_type(self):
        return self.marker_type

    def on_save_map_event(self, html):
        file_name, _ = QFileDialog.getSaveFileName(self, tr_texts["text_save"][self.lang], options=QFileDialog.DontUseNativeDialog)
        if not file_name:
            return
        file_name = file_name + '.html' if not file_name.endswith('.html') else file_name
        if file_name:
            page = self.widget_map.embed_markers(html, self.get_marker_type())
            with open(file_name, 'w', encoding='utf8') as m:
                m.write(page)

    def date_range1_validator(self):
        try:
            self.date_range2.setMinimumDate(self.date_range1.date())
        except Exception as e:
            print(e)
    def date_range2_validator(self):
        try:
            self.date_range1.setMaximumDate(self.date_range2.date())
        except Exception as e:
            print(e)

    def get_date_range(self, date1, date2):
        initial_year = date1.year()
        initial_month = date1.month()
        initial_day = date1.day()
        final_year = date2.year()
        final_month = date2.month()
        final_day = date2.day()
        return (initial_year, initial_month, initial_day), (final_year, final_month, final_day)

class Cloud(QWidget):
    def __init__(self, parent: typing.Optional['QWidget'], flags: typing.Union[Qt.WindowFlags, Qt.WindowType], tweets_data, mode: str = None, lang="it") -> None:
        super(Cloud, self).__init__(parent=parent, flags=flags)
        self.mode = mode
        self.tweets_data = tweets_data
        self.lang = lang
        self.setWindowTitle(tr_texts["text_generate_cloud"][self.lang])
        self.resize(500, 400)

        self.wc = generate_wordcloud(self.tweets_data)

        qim = ImageQt(self.wc.to_image())
        self.cloud_image = QPixmap.fromImage(qim)
        self.cloud_label = QLabel()
        self.cloud_label.setPixmap(self.cloud_image)

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.cloud_label)

        self.button_save_cloud = QPushButton()
        self.button_save_cloud.setText(tr_texts["text_save"][self.lang])
        self.button_save_cloud.clicked.connect(self.button_salva_event)

        self.layout.addSpacing(30)
        self.layout.addWidget(self.button_save_cloud)
        self.layout.setAlignment(Qt.AlignCenter)
        self.setLayout(self.layout)

    def button_salva_event(self):
        file_name, _ = QFileDialog.getSaveFileName(self, tr_texts["text_save"][self.lang], options=QFileDialog.DontUseNativeDialog) \
            if self.mode != 'testing' else ('test', None)
        if not file_name:
            return
        else:
            file_name = file_name + '.png' if not file_name.endswith('.png') else file_name
            self.wc.to_file(file_name)

class Stats(QWidget):
    def __init__(self, parent: typing.Optional['QWidget'], flags: typing.Union[Qt.WindowFlags, Qt.WindowType], tweets_data, mode: str = None, lang="it") -> None:
        super(Stats, self).__init__(parent=parent, flags=flags)
        self.mode = mode
        self.tweets_data = tweets_data
        self.lang = lang
        self.setWindowTitle(tr_texts["text_generate_hist"][self.lang])
        self.resize(1000, 600)

        hist = generate_hashtag_histogram(self.tweets_data)
        buf = io.BytesIO()
        hist.savefig(buf, bbox_inches="tight")
        self.image = PIL.Image.open(buf)
        self.image = PIL.ImageQt.ImageQt(self.image)
        self.hist_image = QPixmap.fromImage(self.image)

        self.hist_label = QLabel()
        self.hist_label.setAlignment(Qt.AlignTop)
        self.hist_label.setPixmap(self.hist_image)

        self.stat_layout = QHBoxLayout()
        self.stat_layout.addWidget(self.hist_label)
        self.rank_label = QLabel()
        self.rank_label.setFont(QFont(self.rank_label.font().rawName(), 16))
        self.rank_label.setText("<p><b>Hashtag Ranking:</b></p><ul>")
        for p in get_hashtags_ranking(self.tweets_data):
            self.rank_label.setText(self.rank_label.text()+"<li>"+p[0]+": "+str(p[1])+"</li>")
        self.rank_label.setText(self.rank_label.text() + "</ul>")
        self.rank_label.setAlignment(Qt.AlignTop)
        self.stat_layout.addSpacing(60)
        self.stat_layout.addWidget(self.rank_label)

        self.stats_label = QLabel()
        self.stats_label.setFont(QFont(self.stats_label.font().rawName(), 16))
        self.stats_label.setText(f"<p><b>Numero di Tweet: </b>{len(tweets_data)}</p>")
        if(avg_time_tweets(tweets_data) is not None):
            self.stats_label.setText(self.stats_label.text() + f"<p><b>Tweet/h: </b>{round(avg_time_tweets(tweets_data),2)}</p>")
        self.stats_label.setText(self.stats_label.text() + f"<p><b>Tweet con posizione: </b>{num_geo_tweets(tweets_data)}</p>")
        perc="0"
        if perc_geo_tweets(tweets_data) is not None:
            perc = str(round(perc_geo_tweets(tweets_data), 2))
        self.stats_label.setText(self.stats_label.text() + f"<p><b>Tweet con posizione (%): </b>{perc}%</p>")
        self.stats_label.setText(self.stats_label.text() + f"<p><b>Tweet con immagini: </b>{num_img_tweets(tweets_data)}</p>")
        self.stats_label.setAlignment(Qt.AlignTop)
        self.stat_layout.addSpacing(60)
        self.stat_layout.addWidget(self.stats_label)


        self.layout = QVBoxLayout()
        self.layout.addSpacing(30)
        self.layout.addLayout(self.stat_layout)


        self.button_save_stats = QPushButton()
        self.button_share = QPushButton()
        self.button_share.setText(tr_texts["text_share"][self.lang])
        self.button_save_stats.setText(tr_texts["text_save"][self.lang])
        self.button_save_stats.clicked.connect(self.button_salva_event)
        self.layout.addSpacing(30)
        self.layout.addWidget(self.button_save_stats)
        self.layout.addWidget(self.button_share)

        self.button_share.clicked.connect(self.button_share_event)

        self.setLayout(self.layout)

        self.posted_message = None

    def button_salva_event(self):
        file_name, _ = QFileDialog.getSaveFileName(self, tr_texts["text_save"][self.lang],
                                                   options=QFileDialog.DontUseNativeDialog) \
            if self.mode != 'testing' else ('test', None)
        if not file_name:
            return
        else:
            file_name = file_name + '.png' if not file_name.endswith('.png') else file_name
            self.image.save(file_name)

    def button_share_event(self):
        self.api = InterfacciaAPI()
        self.api.auto_post_on_twitter(self.tweets_data)
        self.posted_message = self.posted_message or QErrorMessage(self)
        self.posted_message.setWindowTitle(tr_texts["text_success"][self.lang])
        self.posted_message.showMessage(tr_texts["text_confirm_share"][self.lang])



if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = Dashboard(None, Qt.WindowType.Window)
    apply_stylesheet(app, 'light_blue.xml')
    app.exec_()
