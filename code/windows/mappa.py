from typing import List, Tuple, Optional, Union
import sys
import base64
from PyQt5 import QtWidgets
from qt_material import apply_stylesheet
from PyQt5.QtWebEngineWidgets import QWebEngineView
from PyQt5.QtWidgets import QFileDialog
from windows.mappa_html import mappa_html


class Mappa(QWebEngineView):

    def __init__(self, parent: Optional[QtWidgets.QWidget]) -> None:
        super().__init__(parent=parent)

        self.map_id = self.setup_map()
        self.update_map()
        self.queue = []
        self.coo_line = []
        self.is_loaded = False

        self.markers = []
        self.polyline = []

    def update_map(self):
        webpage = base64.b64decode(mappa_html).decode()
        self.setHtml(webpage)
        self.loadFinished.connect(self.load_handler)

    def add_marker(self, coordinates: List[float]):
        if len(coordinates) != 2:
            return

        if self.is_loaded:
            self.markers.append(coordinates)
            javascript_marker = f"""
                markers.push(L.marker({list(coordinates)}).addTo({self.map_id}));
            """
            self.page().runJavaScript(javascript_marker)
        else:
            self.queue.append(coordinates)

    def add_polyline(self, coordinates):
        if self.is_loaded:
            self.polyline = coordinates
            javascript_line = f"""
                polyline.push(L.polyline({coordinates}, {{edit_with_drag: true}}).addTo({self.map_id}));
            """
            self.page().runJavaScript(javascript_line)
        else:
            self.coo_line = coordinates

    def remove_polyline(self):
        self.polyline = []
        javascript_line = f"""if(typeof polyline !== 'undefined'){{ for (i = 0; i < polyline.length; i++) {{
                        {self.map_id}.removeLayer(polyline[i]);
                    }}}}"""
        self.page().runJavaScript(javascript_line)

    def remove_markers(self):
        self.markers = []
        javascript_marker = f"""for (i = 0; i < markers.length; i++) {{
                {self.map_id}.removeLayer(markers[i]);
            }}"""

        self.page().runJavaScript(javascript_marker)

    def setup_map(self):
        return "map_55d3d27af3d6489e87383f61da894c6a"

    def load_handler(self):
        self.is_loaded = True
        javascript_marker = f"markers = []; polyline = [];"""
        self.page().runJavaScript(javascript_marker)
        for t in self.queue:
            self.add_marker(t)
            self.markers.append(t)
        self.queue = []
        self.add_polyline(self.coo_line)

    def embed_markers(self, html: str, marker_type):

        page, end = html.split('</body>')

        map_to_hide = '<div class="leaflet-pane leaflet-map-pane" style='
        display = '"display:none;'

        page = page.replace(map_to_hide, map_to_hide + display)

        if marker_type == 'marker':
            markers = self.markers
        elif marker_type == 'polyline':
            markers = self.polyline
        else:
            markers = None

        script = ''
        if len(markers) > 0:
            script = f"""<script>
                let saveMarkers = {repr(markers)} || []
            """

            script += f"""
                for ( let i = 0; i < saveMarkers.length; i++ ) {{
                    const marker = saveMarkers[i];
                    L.marker(marker).addTo({self.map_id});
                }}
                if (saveMarkers.length > 0 ) {{
                    {self.map_id}.fitBounds(saveMarkers);
                }}
                {self.map_id}.setZoom(3)
            """

            if marker_type == 'polyline':
                script += f"""
                    let polylineObj = L.polyline(saveMarkers, {{edit_with_drag: true}}).addTo({self.map_id});
            """

            script += "</script>"

        return f'{page}{script}</body>{end}'


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    window = Mappa(None)

    def add_test_points(finished):
        if finished:
            points = [
                [-0.6420702, -65.92357199],
                [-2.86682115, -82.62172817],
                [11.72895779, -78.30388846],
                [-66.72726205,-0.82511361],
                [0.20415289,129.65200275],
                [-28.87290271,-80.25334187],
                [-2.47880238,-98.39087828],
                [14.02498283,-8.40036459],
                [-14.47869132,35.60688358],
                [84.25280417,-47.30880409]]
            for point in points:
                window.add_marker(point)
    window.loadFinished.connect(add_test_points)
    window.show()
    apply_stylesheet(app, 'light_blue.xml')
    app.exec_()



