import sys
from PyQt5.QtCore import QRect, Qt
from PyQt5.QtGui import QFont

from PyQt5.QtWidgets import *
from qt_material import apply_stylesheet

from windows.monitora import Monitora
from windows.traccia import Traccia
from utils.utils import reload_search
from utils.translation import tr_texts
from utils.translation import tr_lang
from windows.tweet import Tweet


class Menu(QWidget):

    def __init__(self) -> None:
        super().__init__()

        self.setWindowTitle('Tweet eye')

        self.label_title = QLabel(self)
        self.label_title.setText('Tweet Eye')
        self.label_title.setFont(QFont(self.label_title.font().rawName(), 30))

        title_layout = QHBoxLayout()
        title_layout.addWidget(self.label_title)
        title_layout.setAlignment(Qt.AlignCenter)

        self.button_track = QPushButton(tr_texts["text_monitor"][tr_lang], self)
        self.button_track.clicked.connect(self.open_track)
        self.button_track.setMinimumSize(150, 70)

        self.button_track_user = QPushButton(tr_texts["text_track"][tr_lang])
        self.button_track_user.clicked.connect(self.open_track_user)
        self.button_track_user.setMinimumSize(150, 70)

        self.button_load = QPushButton(tr_texts["text_load"][tr_lang])
        self.button_load.clicked.connect(self.load_file)
        self.button_load.setMinimumSize(150, 70)

        self.select_lang = QComboBox()
        font_combo = self.select_lang.font()
        font_combo.setPointSize(14)
        self.select_lang.setFont(font_combo)
        self.select_lang.addItems(["it", "en"])
        self.select_lang.currentIndexChanged.connect(self.change_language)

        button_layout = QHBoxLayout()
        button_layout.addStretch()
        button_layout.addWidget(self.button_track)
        button_layout.addSpacing(60)
        button_layout.addWidget(self.button_track_user)
        button_layout.addSpacing(60)
        button_layout.addWidget(self.button_load)

        button_layout.addSpacing(60)
        button_layout.addWidget(self.select_lang)

        button_layout.addStretch()



        layout = QVBoxLayout()
        layout.addSpacing(30)
        layout.addLayout(title_layout)
        layout.addSpacing(60)
        layout.addLayout(button_layout)
        layout.addStretch()
        self.setLayout(layout)

        self.window_width, self.window_height = 1200, 650
        self.setMinimumSize(self.window_width, self.window_height)

        self.setAttribute(Qt.WidgetAttribute.WA_DeleteOnClose)

        self.show()

    def open_track(self):

        self.monitora = Monitora(self, Qt.WindowType.Window, lang=tr_lang)
        self.monitora.show()

    def open_track_user(self):

        self.traccia = Traccia(self, Qt.WindowType.Window, lang=tr_lang)
        self.traccia.show()

    def load_file(self):
        file_name, _ = QFileDialog.getOpenFileName(self,tr_texts["text_select_file"][tr_lang], "",  "JSON (*.json)")
        if not file_name:
            return
        tweets, type_t = reload_search(file_name)
        if type_t == "M":
            self.monitora = Monitora(self, Qt.WindowType.Window, lang=tr_lang)
            self.monitora.show()
            for t in tweets:
                media = None
                if 'media' in t.entities:
                    media = [image["media_url"] for image in t.entities["media"]][0]
                self.monitora.add_tweet(Tweet(t.user.screen_name, str(t.created_at), t.text, media), t)
        elif type_t == "T":
            self.traccia = Traccia(self, Qt.WindowType.Window, lang=tr_lang)
            self.traccia.show()
            for t in tweets:
                media = None
                if 'media' in t.entities:
                    media = [image["media_url"] for image in t.entities["media"]][0]
                self.traccia.add_tweet(Tweet(t.user.screen_name, str(t.created_at), t.text, media), t)
            self.traccia.add_polyline()

    def change_language(self):
        global tr_lang
        tr_lang = self.select_lang.currentText()
        self.button_track_user.setText(tr_texts["text_track"][tr_lang])
        self.button_track.setText(tr_texts["text_monitor"][tr_lang])
        self.button_load.setText(tr_texts["text_load"][tr_lang])

if __name__ == '__main__':
    app = QApplication(sys.argv)

    page = Menu()

    # extra = {
    #     'danger': '#dc3545',
    #     'warning': '#ffc107',
    #     'success': '#17a2b8',
    # }

    apply_stylesheet(app, 'light_blue.xml')

    sys.exit(app.exec_())
