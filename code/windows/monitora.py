import typing
from PyQt5 import QtCore
from PyQt5.QtWidgets import *
from PyQt5.QtCore import QTimer
from PyQt5.QtGui import QIntValidator

import sys
from qt_material import apply_stylesheet

from interfacce.interfaccia_real_time import InterfacciaRT
from windows.dashboard import Dashboard
from interfacce.interfaccia_API import InterfacciaAPI
from windows.tweet import Tweet

from utils.utils import save_search
from utils.translation import tr_texts
from pathlib import Path

from utils.utils import avg_time_tweets


class Monitora(Dashboard):

    def __init__(self, parent: typing.Optional['QWidget'], flags: typing.Union[QtCore.Qt.WindowFlags, QtCore.Qt.WindowType], mode : str = None, lang="it") -> None:
        self.button_place = QPushButton()
        self.place_radius = QLineEdit()
        super(Monitora, self).__init__(parent=parent, flags=flags, mode=mode, lang=lang)
        self.set_title_text(tr_texts["text_monitor"][self.lang])
        self.set_placeholder_text(tr_texts["text_monitor_placeholder"][self.lang])

        self.line_edit.setObjectName('Hashtag')

        self.line_edit_place = QLineEdit()
        self.line_edit_place.setPlaceholderText(tr_texts['text_place_placeholder'][self.lang])

        self.line_edit_place_error_message = None

        self.place_radius.setValidator(QIntValidator())
        self.place_radius.setPlaceholderText(tr_texts['text_place_radius'][self.lang])

        self.header_horizontal_layout.insertWidget(3, self.line_edit_place)
        self.header_horizontal_layout.insertWidget(4, self.place_radius)

        self.n_alarm = QLineEdit()
        self.n_alarm.setValidator(QIntValidator())
        self.n_alarm.setPlaceholderText(tr_texts['text_alarm_placeholder'][self.lang])
        self.header_horizontal_layout.insertWidget(6, self.n_alarm)
        self.header_horizontal_layout.insertWidget(7, self.button_real_time)

        self.tweets_alarm_message = None

        self.marker_type = 'marker'

    def set_text(self):

        self.button_save_map.setText(tr_texts["text_save_map"][self.lang])
        self.button_hist.setText(tr_texts["text_generate_hist"][self.lang])
        self.button_cloud.setText(tr_texts["text_generate_cloud"][self.lang])
        self.button_salva.setText(tr_texts["text_save"][self.lang])

        self.button_real_time.setText(tr_texts["text_start_realtime"][self.lang])
        self.button_real_time.clicked.connect(self.button_avvia_real_time_event)

        self.button_avvia.setText(tr_texts["text_start"][self.lang])

    def button_avvia_event(self):
        place = self.line_edit_place.text()

        if not place and not super(Monitora, self).button_avvia_event():
            return
        self.api = InterfacciaAPI()
        inital_date, final_date = self.get_date_range(self.date_range1.date(),self.date_range2.date())
        self.api.set_range(inital_date, final_date)

        if place:
            radius = max(10, int(self.place_radius.text() or 0))
            content = self.line_edit.text() or ""
            self.api.get_tweets_by_place(place_name=place, radius=radius, content=content)
        else:
            self.api.get_tweets_by_content(self.line_edit.text())

        self.remove_tweets()
        self.remove_markers()
        for t in self.api.tweets:
            media = None
            if 'media' in t.entities:
                media = [image["media_url"] for image in t.entities["media"]][0]
            tweet = Tweet(t.user.screen_name, str(t.created_at), t.text, media)
            self.add_tweet(tweet, t)

    def button_avvia_real_time_event(self):
        if not super(Monitora, self).button_avvia_event():
            return
        self.api = InterfacciaRT()

        self.api.start_real_time(self.line_edit.text())
        self.remove_tweets()
        self.remove_markers()
        self.timer = QTimer()
        self.timer.timeout.connect(self.update_tweet)

        self.button_avvia.setDisabled(True)
        self.button_place.setDisabled(True)
        self.place_radius.setDisabled(True)
        self.button_real_time.setText(tr_texts["text_interrupt"][self.lang])
        self.button_real_time.clicked.disconnect()
        self.button_real_time.clicked.connect(self.button_avvia_real_time_stop)

        self.timer.start(1000)

    def button_avvia_real_time_stop(self):

        self.timer.stop()
        self.api.stop_real_time()
        self.button_avvia.setDisabled(False)
        self.button_place.setDisabled(False)
        self.place_radius.setDisabled(False)
        self.button_real_time.setText(tr_texts["text_start_realtime"][self.lang])
        self.button_real_time.clicked.disconnect()
        self.button_real_time.clicked.connect(self.button_avvia_real_time_event)

    def update_tweet(self):
        new_tweets = self.api.get_new_tweets()
        if new_tweets is not False:
            for t in new_tweets:
                media = None
                if 'media' in t.entities:
                    media = [image["media_url"] for image in t.entities["media"]][0]
                tweet = Tweet(t.user.screen_name, str(t.created_at), t.text, media)
                self.add_tweet_reverse(tweet, t)

            if (self.n_alarm.text() is not None and self.n_alarm.text() != ""):
                threshold = (max(0, int(self.n_alarm.text())))
                if len(self.tweets_data) > 4:
                    if avg_time_tweets(self.tweets_data[-4:]) is not None and threshold <= avg_time_tweets(self.tweets_data[-4:]):
                        self.tweets_alarm_message = self.tweets_alarm_message or QErrorMessage(self)
                        if not self.tweets_alarm_message.isVisible():
                            self.tweets_alarm_message.setWindowTitle(tr_texts["text_alarm"][self.lang])
                            self.tweets_alarm_message.showMessage(tr_texts["text_alarm_description"][self.lang])

    def button_salva_event(self):

        absolute_file = super(Monitora, self).button_salva_event()
        if not absolute_file:
            return

        folder, file = Path(absolute_file).parent, Path(absolute_file).name
        save_search('M', self.tweets_data, folder, file)

    def closeEvent(self, event):
        try:
            self.api.stop_real_time()
        except:
            pass
        super(Monitora, self).closeEvent(event)




if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = Monitora(None, QtCore.Qt.WindowType.Window)
    apply_stylesheet(app, 'light_blue.xml')
    app.exec_()