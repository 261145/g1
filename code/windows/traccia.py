import typing
from PyQt5 import QtCore
from PyQt5.QtWidgets import *

import sys
from qt_material import apply_stylesheet

from windows.dashboard import Dashboard
from interfacce.interfaccia_API import InterfacciaAPI
from windows.tweet import Tweet

from utils.utils import save_search, get_geo_coordinates
from utils.translation import tr_texts
from pathlib import Path


class Traccia(Dashboard):

    def __init__(self, parent: typing.Optional['QWidget'], flags: typing.Union[QtCore.Qt.WindowFlags, QtCore.Qt.WindowType], mode : str = None, lang="it") -> None:
        super(Traccia, self).__init__(parent=parent, flags=flags, mode=mode, lang=lang)
        self.set_title_text(tr_texts["text_track"][self.lang])
        self.set_placeholder_text(tr_texts["text_track_placeholder"][self.lang])

        self.line_edit.setObjectName('Utente')

        self.api = InterfacciaAPI()
        self.marker_type = 'polyline'

    def button_avvia_event(self):
        if not super(Traccia, self).button_avvia_event():
            return
        users = self.api.get_users_list(self.line_edit.text())
        inital_date, final_date = self.get_date_range(self.date_range1.date(), self.date_range2.date())
        self.api.set_range(inital_date, final_date)
        if len(users) > 0:
            user = users[0]
            self.api.get_user_tweets(user.screen_name)
            self.remove_tweets()
            self.remove_markers()
            for t in self.api.tweets:
                media = None
                if 'media' in t.entities:
                    media = [image["media_url"] for image in t.entities["media"]][0]
                tweet = Tweet(user.screen_name, str(t.created_at), t.text, media)
                self.add_tweet(tweet, t)

            self.add_polyline()
        else:
            self.tweets_error_message = self.tweets_error_message or QErrorMessage(self)
            self.tweets_error_message.setWindowTitle(tr_texts["text_error"][self.lang])
            self.tweets_error_message.showMessage(tr_texts["text_user_not_found"][self.lang])
            return None

    def add_polyline(self):
        coo = []
        for t in self.tweets_data:
            c = get_geo_coordinates(t)
            if c is not None:
                coo.append(c)
        self.remove_polyline()
        if len(coo) > 1:
            self.widget_map.add_polyline(coo)


    def button_salva_event(self):
        absolute_file = super(Traccia, self).button_salva_event()
        if not absolute_file:
            return

        folder, file = Path(absolute_file).parent, Path(absolute_file).name
        save_search('T', self.tweets_data, folder, file)



if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = Traccia(None, QtCore.Qt.WindowType.Window)
    apply_stylesheet(app, 'light_blue.xml')
    app.exec_()