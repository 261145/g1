from PyQt5.QtWidgets import QVBoxLayout, QLabel, QApplication, QFrame
from PyQt5.QtGui import QImage, QFont, QImageWriter, QPixmap
import sys
from qt_material import apply_stylesheet

import requests


class Tweet(QFrame):
    def __init__(self, user, date, text, image_url):
        super(Tweet, self).__init__()
        self.container = QVBoxLayout()
        self.user = QLabel("@"+user)
        self.user.setFont(QFont('Arial', 11))
        self.user.setStyleSheet("color: #2979ff;")

        self.date = QLabel(date)
        self.date.setFont(QFont('Arial', 9))
        self.date.setStyleSheet("color: #919191;")

        self.content_text = QLabel(text)
        self.content_text.setFont(QFont('Arial', 10))
        self.content_text.setWordWrap(True)

        self.image = QLabel()
        if image_url:
            image_request = requests.get(image_url)
            if image_request.status_code == 200:
                pixel_map = QPixmap()
                pixel_map.loadFromData(image_request.content)

                image_height, image_width = pixel_map.height(), pixel_map.width()
                if image_height > 500:
                    height = 300
                    width = height * image_width // image_height
                elif image_width > 500:
                    width = 500
                    height = width * image_height // image_width
                else:
                    width, height = image_width, image_height

                pixel_map = pixel_map.scaled(width, height)
                self.image.setPixmap(pixel_map)
        self.container.addWidget(self.user)
        self.container.addWidget(self.date)
        self.container.addWidget(self.content_text)
        self.container.addWidget(self.image)

        self.setLayout(self.container)

        self.setStyleSheet('background-color: #ffffff;')


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = Tweet("utente", "18-03-2021", "Aiuto, terremoto!!!")
    window.show()
    apply_stylesheet(app, 'light_blue.xml')
    app.exec_()