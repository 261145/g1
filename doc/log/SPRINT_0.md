### Log incontri sprint 0: totale ore 12
* 10/04/2021 - 15:30-18:30; inizio mockup e documenti preliminari
* 11/04/2021 - 15:00-19:00; gioco scrumble e report del gioco
* 13/04/2021 - 15:00-16:30; pianificazione e divisione del lavoro da fare
* 16/04/2021 - 09:30-10:30; controllo lavoro sul mockup e diagrammi
* 17/04/2021 - 09:30-12:30; Planning poker e creazione backlog
* 17/04/2021 - 15:00-17:00; Ordinamento documenti su repo GitLab
