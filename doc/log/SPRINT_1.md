### Log incontri sprint 1: totale ore 9
* 21/04/2021 - 19:00-20:00; Incontro per organizzazione lavoro
* 23/04/2021 - 18:00-19:00; Incontro per controllo lavoro svolto
* 26/04/2021 - 19:00-20:00; Incontro per controllo integrazione
* 01/05/2021 - 11:00-13:00; Grooming session
* 01/05/2021 - 14:00-18:00; Sprint Review e Sprint Retrospective
