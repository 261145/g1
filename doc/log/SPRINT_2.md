### Log incontri sprint 2: totale ore 11
* 06/05/2021 - 15:00-16:00; Incontro per organizzazione lavoro dopo incontro
* 08/05/2021 - 19:00-20:00; Incontro per controllo lavoro svolto
* 10/05/2021 - 19:00-20:00; Incontro per controllo andamento del lavoro
* 15/05/2021 - 09:00-13:00; Grooming session e Sprint Review
* 15/05/2021 - 14:00-18:00; Sprint Retrospective e organizzazione lavoro
