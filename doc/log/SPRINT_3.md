### Log incontri sprint 3: totale ore 9,5
* 17/05/2021 10:00-13:00; Incontro per organizzare il lavoro da svolgere
* 20/05/2021 19:00-20:00; Incontro per controllo andamento del lavoro
* 22/05/2021 18:00-20:00; Grooming session
* 26/05/2021 19:00-20:00; Incontro per andamento del lavoro
* 29/05/2021 09:30-13:00; Incontro per Sprint retrospective
* 30/05/2021 09:00-10:00; Aggiornamento ultimi documenti
