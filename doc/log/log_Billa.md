### Lavoro sprint 0
* 10/04/2021 17:00-19:00; Progettazione Mockup
* 11/04/2021 15:00-17:00; Progettazione Mockup
* 15/04/2021 17:00-19:00; Schema architetturale
* 17/04/2021 15:00-17:00; Presentazione
* 18/04/2021 15:00-19:00; Scrittura, registrazione e montaggio video

Totale: 12 ore

### Lavoro sprint 1
* 19/04/2021 21:00-23:00; Studio libreria pyqt e altre tecnologie
* 20/04/2021 21:00-23:00; Studio libreria pyqt e altre tecnologie
* 22/04/2021 21:00-23:00; Studio libreria pyqt e altre tecnologie
* 23/04/2021 21:00-23:00; Studio libreria pyqt e altre tecnologie
* 24/04/2021 14:00-18:00; Programmazione GUI pagina principale (con Matteo Faraci)
* 26/04/2021 09:00-12:00; Programmazione scheletro applicazione
* 28/04/2021 16:00-20:00; Programmazione scheletro applicazione
* 28/04/2021 21:00-23:00; Programmazione scheletro applicazione
* 29/04/2021 21:00-23:00; Programmazione GUI Traccia
* 30/04/2021 10:00-12:00; Programmazione GUI Monitora
* 30/04/2021 16:00-18:00; Programmazione GUI Monitora Real Time
* 01/05/2021 18:00-19:00; Programmazione Fix
* 02/05/2021 15:00-18:00; Scrittura, registrazione e montaggio video

Totale: 31 ore

### Lavoro sprint 2
* 03/05/2021 21:00-22:00; Programmazione caricamento utente
* 04/05/2021 22:00-24:00; Programmazione salvataggio monitora
* 05/05/2021 21:00-22:00; Programmazione caricamento monitora
* 06/05/2021 22:00-23:00; Integrazione mappa
* 07/05/2021 15:00-16:00; Integrazione mappa con marker
* 08/05/2021 09:00-13:00; Integrazione mappa com marker
* 09/05/2021 15:00-19:00; Programmazione Fix
* 10/05/2021 21:00-23:00; Programmazione Fix aggiornamento mappa
* 11/05/2021 21:00-23:00; Programmazione rimozione marker mappa
* 12/05/2021 10:00-12:00; Programmazione Fix localizzazione
* 13/05/2021 15:00-18:00; Programmazione mappa tracciamento utente
* 14/05/2021 16:00-19:00; Programmazione Fix (con Matteo Faraci)
* 15/05/2021 17:00-20:00; Scrittura, registrazione e montaggio video

Totale: 29 ore

### Lavoro sprint 3
* 17/05/2021 09:00-12:00; Programmazione multilingua (con Storchi e Gibertoni)
* 17/05/2021 21:00-24:00; Programmazione nuvola wordcloud (Con Storchi)
* 18/05/2021 16:00-19:00; Programmazione finestra statistiche
* 21/05/2021 15:00-18:00; Programmazione aggiunta statistiche
* 22/05/2021 09:00-10:00; Programmazione Fix
* 23/05/2021 15:00-17:00; Programmazione salvataggio wordcloud
* 24/05/2021 17:00-19:00; Programmazione salvataggio istogramma
* 25/05/2021 21:00-23:00; Programmazione Fix
* 27/05/2021 15:00-18:00; Programmazione allarme e post
* 28/05/2021 15:00-18:00; Programmazione filtro date (con Faraci e Storchi)
* 29/05/2021 15:00-18:00; Programmazione fix
* 30/05/2021 17:00-20:00; Scrittura, registrazione e montaggio video

Totale: 31 ore