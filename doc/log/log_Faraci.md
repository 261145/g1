### Sprint 0 - Totale 2 ore e 30 minuti

* 17/04/2014 - 2 ore e 30 minuti. Presentazione PowerPoint e visione lavoro fatto dagli altri membri del gruppo

### Sprint 1 - Totale 13

* 24/04/2021 - 4 ore. Studio documentazione Pyqt5. Test per verificare le conoscenze acquisite.
* 27/04/2021 - 3 ore. Studio documentazione PyQt5.
* 29/04/2021 - 1 ore. Aggiunta chiusura a cascata delle finestre
* 30/04/2021 - 3 ore. Studio documentazione PyQt5. Costruzione interfaccia grafica
* 01/05/2021 - 2 ore. Salvataggio delle ricerche di monitoraggio utente e hashtag

### Sprint 2 - Totale 16

* 03/05/2021 - 5 ore e 30 minuti. Aggiunta mappa nella GUI con markers
* 04/05/2021 - 3 ore. Refractoring dashboard
* 12/05/2021 - 2 ore e 30 minuti. Aggiunta test GUI
* 14/05/2021 - 3 ore. Modifica funzione di aggiunta marker sulla mappa
* 15/05/2021 - 2 ore. Test coverage

### Sprint 3 - Totale 18

* 17/05/2021 - 2 ore. Aggiunta del salvataggio sulla mappa
* 19/05/2021 - 5 ore. Aggiunta delle traduzione e miglioramento salvataggio mappa
* 22/05/2021 - 4 ore e 30 minuti. Aggiunta anteprima immagini ai tweet
* 25/05/2021 - 3 ore e 30 minuti. Aggiunta ricerca per area geografica
* 28/05/2021 - 3 ore. Implementazione nelle GUI della ricerca tramite località
