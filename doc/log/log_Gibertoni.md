### Lavoro Sprint 0 - Totale 9
* 10/04/2021 18:00-20:00; Studio fattibilità del progetto
* 14/04/2021 19:00-20:00; Controllo lavoro del gruppo
* 17/04/2021 10:00-13:00; Aiuto realizzazione documentazione necessaria alla consegna
* 18/04/2021 10:00-13:00; Aiuto realizzazione documentazione necessaria alla consegna

### Lavoro sprint 1 - Totale 8
* 26/04/2021 15:30-17:30; Ripristino dei servizi di GitLab, Taiga e SonarQube
* 27/04/2021 19:00-20:00; Supporto ad organizzazione del team
* 28/04/2021 17:00-18:00; Aiuto manutenzione codice del dev team
* 30/04/2021 09:00-13:00; Preparazione materiale per scrum retrospetive, review e grooming session

### Lavoro Sprint 2 - Totale 23
* 10/05/2021 10:00-13:00; Risoluzione problemi pyinstaller
* 11/05/2021 09:00-10:00; Creazione eseguibile Linux
* 11/05/2021 18:00-20:00; Risoluzione problemi di SonarCloud con Coverage
* 12/05/2021 09:00-10:00; Prova di creazione eseguibile su Windows
* 12/05/2021 18:00-20:00; Creazione eseguibile su Windows con pyinstaller
* 13/05/2021 22:00-24:00; Generazione report coverage
* 14/05/2021 00:00-01:00; Generazione report coverage
* 14/05/2021 11:00-12:00; Fine report coverage
* 14/05/2021 17:00-19:00; Tentativo risoluzione problemi pyinstaller
* 15/05/2021 09:00-13:00; Fix Coverage e lavoro col gruppo prima della consegna
* 15/05/2021 14:00-18:00; Lavoro col gruppo prima della consegna

### Lavoro Sprint 3 - Totale 13
* 17/05/2021 16:00-19:00; Aiuto team di sviluppo per wordcloud
* 21/05/2021 11:00-13:00; Aiuto team di sviluppo
* 26/05/2021 09:00-10:00; Controllo files sul repository
* 29/05/2021 15:00-18:00; Pair programming con GUI
* 30/05/2021 14:00-18:00; Preparazione consegna finale

## Totale ore 53
