### Sprint 0
* 11/4/2021 17.00-19.00 (2h) Redazione documentazione sprint
* 12/4/2021 21.00-22.00 (1h) Redazione documentazione sprint
* 17/4/2021 17.00-19.00 (2h) Redazione documentazione sprint
* 18/4/2021 17.00-19.00 (2h) Redazione documentazione sprint
*Totale 7 ore

### Sprint 1
* 26/4/2021 18.00-19.00 (1h) Ripristino configurazione Taiga
* 27/4/2021 18.00-19.00 (1h) Allineamento configurazione Taiga server Unimore
* 30/4/2021 21.00-22.30 (1,5h) Preparazione per scrum retrospetive, review e grooming session
* 1/5/2021 12.30-13.00 (0,5h) Redazione relazione grooming session
* 1/5/2021 18.00-19.00 (1h) Redazione relazione Sprint review e sprint retrospective
* Totale 5 ore

### Sprint 2
* 5/5/2021 - Ho aggiorna taiga, creato sprint 2 e sprint goal 2 su drive e taiga  (1 ora)
* 12/5/2021 - Ho aggiornato su taiga gli sprint 2 su taiga (1 ora)
* 14/5/2021 - Ho provato primo eseguibile rilasciato (1 ora)
* 15/5/2021 - Revisione documentazione  (2 ora)
* Totale 5 ore

### Sprint 3
* 16/5/2021 - Aggiornamento taiga (1 ora)
* 17/5/2021 - Organizzazione documenti per relazione finale (1 ora)
* 18/5/2021 - Redazione relazione finale (1 ora)
* 19/5/2021 - Redazione relazione finale (1 ora)
* 20/5/2021 - Redazione relazione finale (1 ora)
* 22/5/2021 - Redazione relazione grooming session (1 ora)
* 24/5/2021 - Redazione relazione finale (1 ora)
* 26/5/2021 - Redazione relazione finale (1 ora)
* 27/5/2021 - Redazione relazione finale (1 ora)
* 28/5/2021 - Redazione relazione finale (1 ora)
* 29/5/2021 - Redazione documentazione finale (2 ore)
* Totale 12 ore
