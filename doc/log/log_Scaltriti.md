### Lavoro sprint 0 - Totale 8.5

* 10/04/2021 17:00-19:00; realizzazione del mockup iniziale
* 17/04/2021 9:00-13:00; realizzazione dei diagrammi di attività e di sequenza
* 17/04/2021 16:00-17:00; realizzazione presentazione del progetto
* 18/04/2021 15:00-16:30; creazione video sprint review

### Lavoro sprint 1 - Totale 12.5

* 24/04/2021 16:30-21:30; studio API Twitter e aggiunta logica di ricerca per date
* 25/04/2021 10:00-12:00; salvataggio grezzo di ricerca in file JSON
* 29/04/2021 18:00-19:00; visualizzazione codice prodotto dal team e comunicazione con Twitter per rilascio API 2.0
* 30/04/2021 11:00-13:00; pair programming (Storchi) creata e testata funzione per salvataggio tweet in locale e rifiniture su recupero tweet scaricati
* 01/05/2021 18:00-19:00; aggiunti controlli sul salvataggio del file JSON e pair programming (Billa, Faraci) per perfezionamento del recupero dei tweet
* 02/05/2021 15:30-17:00; supporto creazione video sprint review (Billa)

### Lavoro sprint 2 - Totale 12 

* 03/05/2021 18:15-20:00; installazione front-end e aggiunti funzione e relativo test per caricamento di file di una precedente ricerca
* 09/05/2021 18:00-19:15; installazione pyinstaller per generazione eseguibile e relativo trubleshooting
* 11/05/2021 18:45-19:30; trubleshooting dependency python e pyinstaller
* 12/05/2021 20:00-21:15; generazione tweet di prova
* 14/05/2021 10:30-13:00 & 16:30-20:00; generazione tweet di prova, realizzazione test mock e installazione eseguibile
* 15/05/2021 18:30-19:30; creazione video sprint review (Billa)

### Lavoro sprint 3 - Totale 10.5

* 17/05/2021 21:30-00:00; generazione nuovi tweets e aggiunta funzioni statistiche
* 19/05/2021 21:45-23:15; generazione nuovi tweets e aggiunta test funzioni statistiche
* 21/05/2021 15:00-16:00; funzione per post automatico su Twitter
* 25/05/2021 14:00-16:00; Refactoring funzioni esistenti e completamento funzione per post automatico su Twitter
* 28/05/2021 16:30-18:30; implementazione funzione di auto-post di un tweet con immagine e relativo test
* 29/05/2021 14:00-14.30; generalizzazione funzione di auto-post di un tweet con immagine e relativo test
* 30/05/2021 15:00-16.00; creazione video sprint review