### Lavoro sprint 0 - Totale 6 ore

* 14/04/2021: Ho creato il diagramma dei casi d'uso. (2 ore)

* 15/04/2021: Ho creato lo schema architetturale. (2 ore)

* 17/04/2021: Ho lavorato al primo diagramma di sequenza e ho fatto gli scenari di casi d'uso. (2 ore) 

### Lavoro sprint 1 - Totale 18 ore

* 25/04/2021: Ho studiato la libreria tweepy per interfacciarsi con le API di twitter e ho creato del codice di prova. (3 ore) 

* 26/04/2021: Ho creato la classe "InterfacciaAPI" che serve a supporto della GUI per interfacciarsi con le API utilizzate per ottenere i tweet.
Ho creato le funzioni per la ricerca di un utente, per il download dei tweet di un certo utente e per settare il range di date. (4 ore)

* 27/04/2021: Ho terminato i test per l'interfaccia riguardanti la parte di tracciamento utente e di ricerca e scaricamento di tweet in base al contenuto e al range di date. (1 ora)

* 28/04/2021: Ho creato l'interfaccia real time e i suoi test. (3 ore)

* 29/04/2021: Sono andato avanti con l'implementazione dell'interfaccia real time e ho corretto qualche bug. (1 ora)

* 30/04/2021: Creata funzione e test per la restituzione dei nuovi tweet in real time.
Test di integrazione con GUI. (6 ore)

### Lavoro sprint 2 - Totale 18 ore

* 12/05/2021: Ho studiato il modulo mock di unittest per fare il mocking dei test ed effettuare i test offline (4 ore)

* 13/05/2021: Ho iniziato a mockare i test utilizzando il modulo mock di unittest (6 ore)

* 14/05/2021: Ho terminato il mocking dei test e ho effettuato un refactoring generale del branch backend (8 ore)

### Lavoro sprint 3 - Totale 12 ore

* 17/05/2021: Ho creato le funzioni per restituire la wordcloud, il ranking degli hashtag più twittati e l'istogramma della distribuzione di un hashtag nel tempo e le relative funzioni di testing. (6 ore)

* 21/05/2021: Ho creato le funzioni per la ricerca di tweet in base all'area geografica e alle sottoaeree. (6 ore) 