# Documento di analisi

## Setup: Team 
- Nome del team: G1
- Componenti del team 
    * Product Owner (PO): Fabio Morsiani
    * Scrum Master (SM): Federico Gibertoni
    * Development team (DT): Mattia Billa, Matteo Faraci, Federico Scaltriti, Lorenzo Storchi
- Sprint 0: Morsiani si è occupato della redazione e gestione delle user story e del resoconto della partita di Scrumble, Gibertoni si è occupato dell’organizzazione del gruppo e del setup dei molteplici tools, inoltre si è occupato della redazione delle Definition of Ready e Definition of Done, Billa si è occupato del mockup e organizzazione del video di presentazione, Storchi si è occupato del diagramma dei casi d’uso e schema architetturale (insieme a Gibertoni), Scaltriti dei diagrammi di sequenza e di attività, Faraci si è occupato degli scenari dei casi d'uso e creazione della presentazione del progetto.  
Il development team al completo svolge il planning poker e insieme al PO crea il backlog.  

## Documentazione
### Descrizione della soluzione proposta
Si vuole realizzare un software desktop per permettere ad un utente, una volta raccolti, di poter cercare ed analizzare tweet su un argomento a scelta, potrà visualizzarli singolarmente, vedere da dove provengono su una mappa e generare una term cloud. Inoltre il sistema permetterà di monitorare in tempo reale eventi e, in caso di emergenza, di attivare procedure di allarme. Oltre a tutto questo l’utente potrà salvare la sua ricerca in un file per poterla poi consultare altre volte.
Il sistema prevede l’utilizzo di API per la raccolta dei tweet e per il monitoraggio in tempo reale. Le raccolte vengono salvate in locale e possono essere consultabili in qualsiasi momento. Per il monitoraggio in tempo reale l’utente può inserire una serie di termini che il sistema tiene sotto controllo. Per la parte grafica il sistema sfrutta librerie grafiche in python e API per interfacciarsi con le mappe e mette a disposizione una dashboard che permette di selezionare e visualizzare i tweet in diversi modi.

### [Schema architetturale/funzionale](http://pccabri2.mat.unimo.it:9001/261145/G1/-/blob/master/doc/Schema%20architetturale.pdf)

### [Diagramma casi d'uso](http://pccabri2.mat.unimo.it:9001/261145/G1/-/blob/master/doc/Use%20case%20diagram.pdf)

### [Backlog](http://pccabri2.mat.unimo.it:9002/project/admin-g1/backlog)

### [Mockup](http://pccabri2.mat.unimo.it:9001/261145/G1/-/tree/master/doc/mockup.pdf) (da scaricare)

### Criticità

| Criticità | Impatto | Difficoltà |
| --------- | ------- | ---------- |
| Utilizzo librerie di Twitter | Alto | Media |
| Utilizzo libreria grafica Python | Alto | Media |
| Uso API mappe | Medio | Alta |
| Uso libreria per term cloud | Medio | Alta |
| Monitoraggio in tempo reale | Alto | Alta |
