import sys
import unittest
from PyQt5.QtWidgets import QApplication
from PyQt5.QtTest import QTest
from PyQt5.QtCore import Qt
from code.windows.dashboard import Dashboard
from qt_material import apply_stylesheet

app = QApplication(sys.argv)
apply_stylesheet(app, 'light_blue.xml')


class TestDashboard(unittest.TestCase):

    def setUp(self) -> None:
        self.window = Dashboard(None, Qt.WindowType.Window)

    def test_default(self):
        self.assertEqual(self.window.line_edit.text(), "")
        self.assertEqual(len(self.window.tweets), 0)

    def test_avvia_button_click(self):
        self.window.line_edit.setText('')
        QTest.mouseClick(self.window.button_avvia, Qt.LeftButton)
        self.assertNotEqual(self.window.line_edit_error_message, None)

    def test_button_save(self):
        self.window.line_edit.setText('')
        QTest.mouseClick(self.window.button_salva, Qt.LeftButton)
        self.assertNotEqual(self.window.tweets_error_message, None)

    def test_avvia_button_click_with_content(self):
        self.window.line_edit.setText('basket')
        QTest.mouseClick(self.window.button_avvia, Qt.LeftButton)
        self.assertEqual(self.window.line_edit_error_message, None)


if __name__ == "__main__":
    unittest.main()
