import datetime
import json
import unittest
from pathlib import Path
from unittest.mock import patch
import tweepy
from code.interfacce.interfaccia_API import InterfacciaAPI
from PIL import Image

USERS_LIST = []
USER_TWEETS = []
SEARCH_BY_CONTENT = []
SEARCH_BY_CONTENT_WITH_RANGE = []
SEARCH_WITH_COORDINATES = []


def load_mock_users_list():
    with open(Path(__file__).parent/'mock_users_list.json', "r", encoding='utf-8') as json_file:
        data = json.load(json_file)

    api = InterfacciaAPI(no_auth=True)
    for u in data["users"]:
        user = tweepy.Status.parse(api, u)
        USERS_LIST.append(user)


def load_mock_user_tweets():
    with open(Path(__file__).parent/'mock_user_tweets.json', "r", encoding='utf-8') as json_file:
        data = json.load(json_file)

    api = InterfacciaAPI(no_auth=True)
    for t in data["tweets"]:
        tweet = tweepy.Status.parse(api, t)
        USER_TWEETS.append(tweet)


def load_mock_search_by_content():
    with open(Path(__file__).parent/'mock_search_by_content.json', "r", encoding='utf-8') as json_file:
        data = json.load(json_file)

    api = InterfacciaAPI(no_auth=True)
    for t in data["tweets"]:
        tweet = tweepy.Status.parse(api, t)
        SEARCH_BY_CONTENT.append(tweet)


def load_mock_search_by_content_with_range():
    with open(Path(__file__).parent/'mock_search_by_content_with_range.json', "r", encoding='utf-8') as json_file:
        data = json.load(json_file)

    api = InterfacciaAPI(no_auth=True)
    for t in data["tweets"]:
        tweet = tweepy.Status.parse(api, t)
        SEARCH_BY_CONTENT_WITH_RANGE.append(tweet)


def load_mock_search_with_coordinates():
    with open(Path(__file__).parent/'mock_search_by_user.json', "r", encoding='utf-8') as json_file:
        data = json.load(json_file)

    api = InterfacciaAPI(no_auth=True)
    for t in data["tweets"]:
        tweet = tweepy.Status.parse(api, t)
        SEARCH_WITH_COORDINATES.append(tweet)


class TestInterfaccia(unittest.TestCase):

    def setUp(self) -> None:
        load_mock_users_list()
        load_mock_user_tweets()
        load_mock_search_by_content()
        load_mock_search_by_content_with_range()
        load_mock_search_with_coordinates()

    def test_instance(self):
        api = InterfacciaAPI(no_auth=True)
        self.assertIsInstance(api, InterfacciaAPI)

    def test_range(self):
        api = InterfacciaAPI(no_auth=True)
        self.assertDictEqual(api.range, {'inizio': datetime.date(2006, 3, 21), 'fine': datetime.date.today() + datetime.timedelta(days = 1)})

    def test_auto_post_image(self):
        image = Image.new("RGB", (100, 100))
        api = InterfacciaAPI()
        text = "Testo di test"
        api.auto_post_image(image, status=text)
        assert self.assertRaises(tweepy.TweepError), False

    @patch('tweepy.API.search_users')
    def test_users_list_default(self, mock_search_users):
        api = InterfacciaAPI(no_auth=True)
        mock_search_users.return_value = USERS_LIST
        users_list = api.get_users_list('nasa')
        self.assertEqual(len(users_list), 10)

    @patch('tweepy.API.search_users')
    def test_users_list_number_of_users(self, mock_search_users):
        api = InterfacciaAPI(no_auth=True)
        mock_search_users.return_value = USERS_LIST
        users_list = api.get_users_list('nasa', 3)
        self.assertEqual(len(users_list), 3)

    @patch('tweepy.Cursor.items')
    @patch('tweepy.API.search_users')
    def test_user_tweets(self, mock_search_users, mock_user_tweets):
        api = InterfacciaAPI(no_auth=True)
        mock_search_users.return_value = USERS_LIST
        mock_user_tweets.return_value = USER_TWEETS
        user = api.get_users_list('nasa', 3)[0]
        api.get_user_tweets(user.screen_name)
        self.assertGreater(len(api.tweets), 0)

    def test_set_range(self):
        api = InterfacciaAPI(no_auth=True)
        old_range = api.range
        api.set_range((2021, 3, 3), (2021, 3, 13))
        self.assertNotEqual(old_range, api.range)

    @patch("tweepy.Cursor.items")
    def test_content_tweet_all(self, mock_search_by_content):
        api = InterfacciaAPI(no_auth=True)
        mock_search_by_content.return_value = SEARCH_BY_CONTENT
        api.get_tweets_by_content("#basket")
        self.assertGreater(len(api.tweets), 0)

    @patch("tweepy.Cursor.items")
    def test_content_tweet_range(self, mock_search_by_content):
        api = InterfacciaAPI(no_auth=True)
        mock_search_by_content.return_value = SEARCH_BY_CONTENT_WITH_RANGE
        api.set_range((2021, 4, 3), (2021, 4, 5))
        api.get_tweets_by_content("#basket")
        self.assertGreater(len(api.tweets), 0)

    @patch("tweepy.Cursor.items")
    def test_tweet_by_place_with_coordinate(self, mock_coordinates):
        api = InterfacciaAPI(no_auth=True)
        mock_coordinates.return_value = SEARCH_WITH_COORDINATES
        api.get_tweets_by_place(60.167347, 24.943071)
        self.assertGreater(len(api.tweets), 0)

    @patch("tweepy.Cursor.items")
    def test_tweet_by_place_with_name(self, mock_coordinates):
        api = InterfacciaAPI(no_auth=True)
        mock_coordinates.return_value = SEARCH_WITH_COORDINATES
        api.get_tweets_by_place(place_name="Modena")
        self.assertGreater(len(api.tweets), 0)

    def test_tweet_by_place_with_wrong_name(self):
        api = InterfacciaAPI(no_auth=True)
        err = api.get_tweets_by_place(place_name="")
        self.assertIsNone(err)


if __name__ == '__main__':
    unittest.main()
