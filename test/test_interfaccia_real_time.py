import json
import unittest
from unittest.mock import patch

from pathlib import Path

import tweepy

from code.interfacce.interfaccia_API import InterfacciaAPI
from code.interfacce.interfaccia_real_time import InterfacciaRT


def load_mock_search_by_content():
    search_by_content = []
    with open(Path(__file__).parent/'mock_search_by_content.json', "r", encoding='utf-8') as json_file:
        data = json.load(json_file)

    api = InterfacciaAPI(no_auth=True)
    for t in data["tweets"]:
        tweet = tweepy.Status.parse(api, t)
        search_by_content.append(tweet)

    return search_by_content


def patch_on_status(stream, status):
    return stream.listener.on_status(status)


class MyTestCase(unittest.TestCase):
    def test_istanza(self):
        stream = InterfacciaRT(no_auth=True)
        self.assertIsInstance(stream, InterfacciaRT)

    @patch("tweepy.streaming.filter")
    def test_stream(self, mock_streaming_filter):
        stream = InterfacciaRT(no_auth=True)
        search_by_content = load_mock_search_by_content()
        mock_streaming_filter.side_effect = stream.listener.on_status(search_by_content[0])
        stream.start_real_time('basket')
        while len(stream.tweets) == 0:
            pass
        stream.stop_real_time()
        self.assertGreater(len(stream.tweets), 0)

    @patch("tweepy.streaming.filter")
    def test_get_new_tweets(self, mock_streaming_filter):
        stream = InterfacciaRT(no_auth=True)
        search_by_content = load_mock_search_by_content()
        i = 0
        mock_streaming_filter.side_effect = patch_on_status(stream, search_by_content[i])
        stream.start_real_time('basket')
        new_tweets = []
        threshold = 10
        while len(new_tweets) < threshold:
            if stream.get_new_tweets() is not False:
                new_tweets.append(stream.get_new_tweets())
            patch_on_status(stream, search_by_content[i])
            i += 1
        stream.stop_real_time()
        self.assertEqual(len(new_tweets), threshold)

    @patch("tweepy.streaming.filter")
    def test_stop_real_time(self, mock_streaming_filter):
        stream = InterfacciaRT(no_auth=True)
        mock_streaming_filter.side_effect = None
        stream.start_real_time('basket')
        stream.stop_real_time()
        self.assertFalse(stream.running)


if __name__ == '__main__':
    unittest.main()
