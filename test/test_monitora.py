import unittest
from test.test_dashboard import TestDashboard
from PyQt5.QtCore import Qt

from code.windows.monitora import Monitora


class TestMonitora(TestDashboard):

    def setUp(self) -> None:
        self.window = Monitora(None, Qt.WindowType.Window, mode='testing')

    def test_avvia_button_click(self):
        super(TestMonitora, self).test_avvia_button_click()


if __name__ == '__main__':
    unittest.main()
