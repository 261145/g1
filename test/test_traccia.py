import unittest
from test import test_dashboard
from code.windows.traccia import Traccia
from PyQt5.QtCore import Qt


class TestTraccia(test_dashboard.TestDashboard):

    def setUp(self) -> None:
        self.window = Traccia(None, Qt.WindowType.Window, mode='testing')

    def test_avvia_button_click(self):
        super(TestTraccia, self).test_avvia_button_click()


if __name__ == '__main__':
    unittest.main()
