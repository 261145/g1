import json

from code.interfacce.interfaccia_API import InterfacciaAPI
from code.utils import utils

from pathlib import Path

import unittest
from unittest.mock import patch
import tweepy
import wordcloud


def load_mock_user_tweets():
    user_tweets = []
    with open(Path(__file__).parent/'mock_search_by_user.json', "r", encoding='utf-8') as json_file:
        data = json.load(json_file)

    api = InterfacciaAPI(no_auth=True)
    for t in data["tweets"]:
        tweet = tweepy.Status.parse(api, t)
        user_tweets.append(tweet)
    return user_tweets


def mock_load_tweets(file_name):
    with open(Path(__file__).parent/file_name, "r", encoding="utf-8") as file:
        return json.load(file)


class MyTestCase(unittest.TestCase):

    @patch('tweepy.Cursor.items')
    def test_save_search(self, mock_tweets):
        api = InterfacciaAPI(no_auth=True)
        search_tweets = mock_load_tweets('mock_search_by_content.json')
        mock_tweets.return_value = search_tweets["tweets"]
        api.get_tweets_by_content('basket')
        self.assertEqual(api.tweets, mock_tweets.return_value)

    @patch('tweepy.Cursor.items')
    def test_save_search_with_range(self, mock_tweets):
        api = InterfacciaAPI(no_auth=True)
        dict_tweets = mock_load_tweets('mock_search_by_content_with_range.json')
        mock_tweets.return_value = dict_tweets["tweets"]
        tweets = dict_tweets["tweets"]
        api.set_range((2021, 4, 3), (2021, 4, 5))
        api.get_tweets_by_content('basket')
        self.assertEqual(api.tweets, tweets)

    def test_reload_search(self):
        tweets, search_type = utils.reload_search(Path(__file__).parent/"mock_user_tweets.json")
        self.assertGreater(len(tweets), 0)

    @patch('tweepy.Cursor.items')
    def test_geo_coordinates(self, mock_user_tweets):
        api = InterfacciaAPI(no_auth=True)
        user_tweets = load_mock_user_tweets()
        mock_user_tweets.return_value = user_tweets
        tweet_coordinates = user_tweets[0].place.bounding_box.coordinates[0][0][::-1]
        api.get_user_tweets("FedericoScaltr1")
        self.assertEqual(utils.get_geo_coordinates(api.tweets[0]), tweet_coordinates)

    def test_get_hashtags_list(self):
        user_tweets = load_mock_user_tweets()
        hashtags = utils.get_hashtags_list(user_tweets)
        print(hashtags)
        self.assertGreater(len(hashtags), 0)

    def test_generate_wordcloud(self):
        user_tweets = load_mock_user_tweets()
        wc = utils.generate_wordcloud(user_tweets)
        self.assertIsNotNone(wc)

    def test_hashtags_ranking(self):
        user_tweets = load_mock_user_tweets()
        r = utils.get_hashtags_ranking(user_tweets)
        self.assertEqual(len(r), 10)

    def test_hashtags_histogram(self):
        user_tweets = load_mock_user_tweets()
        hist = utils.generate_hashtag_histogram(user_tweets)
        self.assertIsNotNone(hist)

    def test_avg_time_tweets(self):
        tweets = load_mock_user_tweets()

        avg = utils.avg_time_tweets(tweets)
        self.assertIsInstance(avg, float)

    def test_num_geo_tweets(self):
        tweets = load_mock_user_tweets()

        count = utils.num_geo_tweets(tweets)
        self.assertIsInstance(count, int)

    def test_perc_geo_tweets(self):
        tweets = load_mock_user_tweets()

        perc = utils.perc_geo_tweets(tweets)
        self.assertGreaterEqual(perc, 0)
        self.assertLessEqual(perc, 100)

    def test_num_img_tweets(self):
        tweets = load_mock_user_tweets()

        num = utils.num_img_tweets(tweets)
        self.assertGreaterEqual(num, 0.0)


if __name__ == '__main__':
    unittest.main()
